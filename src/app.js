import page from 'page';
import checkConnectivity from './network.js';
import { fetchTodos, fetchTodo, createTodo, supprTodo, doneTodo } from './api/todo.js';
import { setTodos, setTodo, getTodos, getTodo, deleteTodo} from './idb.js';
import Details from "./views/details";

checkConnectivity({});
document.addEventListener('connection-changed', e => {
    document.offline = !e.detail;
    if (!document.offline) {
        getTodos().then(value => {
            if (value.synced === "false") {
                createTodo(value).then(() => console.log("Added Todo to db"));
                deleteTodo(value.id).then(() => console.log("Remove Todo to db"));
                value.synced = "true";
                setTodo(value).then(() => console.log("Idb updated"));
            }
        });
    }
});

if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js').then(function(registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}

const app  = document.querySelector('#app .outlet');

fetch('/config.json')
    .then((result) => result.json())
    .then(async (config) => {
        console.log('[todo] Config loaded !!!');
        window.config = config;

        page('/', async () => {
            const module = await import('./views/home.js');
            const Home = module.default;

            const ctn = app.querySelector('[page="Home"]');
            const homeView = new Home(ctn);

            let todos = [];
            if (!document.offline && navigator.onLine) {
                const data = await fetchTodos();
                todos = await setTodos(data);
            } else {
                todos = await getTodos() || [];
            }

            homeView.todos = todos;
            homeView.renderView();
            displayPage('Home');

            // Create todo
            document.addEventListener('create-todo', async ({ detail: todo }) => {
                await setTodo(todo);
                if (!document.offline && navigator.onLine === true) {
                    // If connection is good enough, do the HTTP call
                    const result = await createTodo(todo);
                    if (result !== false) {
                        // If we successfully get a result from API
                        // Get the updated todo list
                        const todos  = await getTodos();
                        // Rerender the template
                        homeView.todos = todos;
                        return homeView.renderView();
                    }
                    // In case of an error
                    // Update the synced flag of the new todo
                    todo.synced = 'false';
                    const todos = await setTodo(todo);
                    // Rerender the template
                    homeView.todos = todos;
                    return homeView.renderView();
                }
            });

            document.addEventListener('update-todo', async ({detail: todo}) => {
                await deleteTodo(todo);
                await doneTodo(todo);
                if (!document.offline && navigator.onLine === true) {
                    const result = await doneTodo(todo.id);
                    if (result !== false) {
                        todo.done = 'true';
                        const todos  = await getTodos();
                        // Rerender the template
                        homeView.todos = todos;
                        todo.done = 'true';
                        return homeView.renderView();
                    }
                    todo.done = 'true';
                    const todos = await doneTodo(todo);
                    // Rerender the template
                    homeView.todos = todos;
                    return homeView.renderView();
                }
            });


            //Delete todo
            document.addEventListener('delete-todo', async ({ detail: todo }) => {
                await deleteTodo(todo);
                if (!document.offline && navigator.onLine === true) {
                    const result = await deleteTodo(todo.id)
                    if (result !== false) {
                        const todos = await supprTodo(todo.id);
                        homeView.todos = todos;
                        return homeView.renderView();
                    }
                    todo.delete = 'true';
                    const todos = await deleteTodo(todo);
                    homeView.todos = todos;
                    return homeView.renderView();
                }
            });
        });

        page('/todos/:id', async (context) => {
            const module = await import('./views/details.js');
            const Details = module.default;

            const ctn = app.querySelector('[page="Details"]');
            const detailsView = new Details(ctn);

            let todo = {};
            if (!document.offline && navigator.onLine) {
                const data = await fetchTodo(context.params.id);
                todo = await fetchTodo(data);
            } else {
                todo = await getTodo(context.params.id) || [];
            }

            detailsView.todo = todo;
            detailsView.renderView();
            displayPage('Details');
        })

        page();
    });

function displayPage(page) {
    const skeleton = document.querySelector('#app .skeleton');
    skeleton.removeAttribute('hidden');
    const pages = app.querySelectorAll('[page]');
    pages.forEach(page => page.removeAttribute('active'));
    skeleton.setAttribute('hidden', 'true');
    const p = app.querySelector(`[page="${page}"]`);
    p.setAttribute('active', true);
}