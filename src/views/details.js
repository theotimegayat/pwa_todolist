import { render, html } from 'lit-html';
import '../component/todo-card';
import 'lit-icon';


export default class Details {
    constructor(page) {
        this.page = page;
        this.properties = {
            todo: ''
        };

        this.renderView();
    }

    set todo(value) {
        this.properties.todo = value;
    }

    get todo() {
        return this.properties.todo;
    }

    template () {
        return html`
      <section class="h-full container px-5 mt-5 mx-auto">
        <a class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 mb-3 rounded inline-flex items-center" href="/">
          <lit-icon icon="arrow_back"></lit-icon>         
          <span class="ml-2">Back</span>
        </a>
          <div class="max-w text-center bg-gray-300 rounded overflow-hidden shadow-lg">
              <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">${this.todo.title}</div>
                <p class="text-gray-700 text-base">
                  ${this.todo.description}
                </p>
                <span class="align-text-bottom">Status : </span><lit-icon icon="${this.todo.done === "true" ? "check" : "close"}"></lit-icon>
              </div>
            </div>
        </main>
      </section>
      <lit-iconset>
        <svg><defs>
          <g id="delete"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"></path></g>
          <g id="cloud-off"><path d="M19.35 10.04C18.67 6.59 15.64 4 12 4c-1.48 0-2.85.43-4.01 1.17l1.46 1.46C10.21 6.23 11.08 6 12 6c3.04 0 5.5 2.46 5.5 5.5v.5H19c1.66 0 3 1.34 3 3 0 1.13-.64 2.11-1.56 2.62l1.45 1.45C23.16 18.16 24 16.68 24 15c0-2.64-2.05-4.78-4.65-4.96zM3 5.27l2.75 2.74C2.56 8.15 0 10.77 0 14c0 3.31 2.69 6 6 6h11.73l2 2L21 20.73 4.27 4 3 5.27zM7.73 10l8 8H6c-2.21 0-4-1.79-4-4s1.79-4 4-4h1.73z"></path></g>
          <g id="send"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"></path></g>
        </defs></svg>
      </lit-iconset>
    `;
    }

    renderView() {
        const view = this.template();
        render(view, this.page);
    }

    /*handleForm(e) {
        e.preventDefault();
        if (this.properties.todo === '') return console.log('[todo] Value is required !!!');
        const todo = {
            id: Date.now(),
            title: this.properties.todo,
            synced: 'true',
            updated: 'false',
            done: 'false',
            deleted: 'false',
            date: Date.now()
        };

        const event = new CustomEvent('create-todo', { detail: todo });
        document.dispatchEvent(event);

        // Clearing input
        this.properties.todo = null;
        const input = document.querySelector('[name="todo"]');
        input.value = '';

        this.renderView();
    }*/
}
